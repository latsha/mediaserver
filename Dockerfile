FROM python:3.5-jessie

COPY . /code

WORKDIR /code

RUN pip install -r /code/mediaserver/requirements.txt

