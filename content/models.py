from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models

from rest_framework.reverse import reverse


class Content(models.Model):
    title = models.CharField(max_length=255)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    counter = models.IntegerField(default=0, blank=True)
    order = models.IntegerField()

    def get_detail_url(self, request=None):
        return reverse('content:details', kwargs={'pk': self.pk}, request=request)

    def __str__(self):
        return "Type: {}, Title: {}".format(self.content_type.model, self.title)


class Video(models.Model):
    video_link = models.URLField()
    subtitle_link = models.URLField()
    videos = GenericRelation(Content)


class Audio(models.Model):
    audio_link = models.URLField()
    bit_rate = models.PositiveIntegerField()
    bits_per_second = models.PositiveIntegerField()
    audios = GenericRelation(Content)


class Text(models.Model):
    text = models.TextField()
    texts = GenericRelation(Content)


class Page(models.Model):
    content = models.ManyToManyField(Content)


NEW = 0
PROCESSING = 1

STATE = ((NEW, 'New'), (PROCESSING, 'Processing'))


class Counter(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE, related_name="views")
    state = models.PositiveSmallIntegerField(choices=STATE)
