from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse

from content.models import Page, Content, Text, Counter


class PageTestCase(APITestCase):
    def setUp(self):
        p = Page.objects.create()
        t = Text.objects.create(text="Hello world")
        c = Content.objects.create(
            content_object=t,
            title="Super content",
            counter=100,
            order=1
        )
        p.content.add(c)

    def test_get_list(self):
        data = {}
        url = reverse("content:list")
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'][0]['content'][0]['counter'], 100)

    def test_detail(self):
        data = {}
        pk = Content.objects.first().pk
        url = reverse("content:details", kwargs={'pk': pk})
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['counter'], 100)

    def test_ensure_counter_incremented(self):
        data = {}
        pk = Content.objects.first().pk
        url = reverse("content:details", kwargs={'pk': pk})
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        cnt = Counter.objects.filter(content__id=pk).count()
        self.assertNotEqual(cnt, 0)
