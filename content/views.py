from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response

from content.models import Page, Content, Counter, NEW
from content.serializers import PageListSerializer, ContentSerializer


class PageList(ListAPIView):
    queryset = Page.objects.all()
    serializer_class = PageListSerializer
    # ordering_fields = ("content__order",)
    ordering = ("content__order",)

    def get_serializer_context(self):
        return {"request": self.request}


class ContentDetail(RetrieveAPIView):
    queryset = Content.objects.all()
    serializer_class = ContentSerializer

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        Counter.objects.create(content=obj, state=NEW)
        serializer = self.get_serializer(obj)
        return Response(serializer.data)
