from django.urls import path

from content.views import PageList, ContentDetail

urlpatterns = [
    path('list/', PageList.as_view(), name='list'),
    path('<int:pk>/', ContentDetail.as_view(), name='details'),
]
