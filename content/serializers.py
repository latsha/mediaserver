from rest_framework.serializers import RelatedField, ModelSerializer, SerializerMethodField

from content.models import Video, Audio, Text, Page, Content


class VideoSerializer(ModelSerializer):

    class Meta:
        model = Video
        fields = ('video_link', 'subtitle_link')


class AudioSerializer(ModelSerializer):

    class Meta:
        model = Audio
        fields = ('audio_link', 'bit_rate', 'bits_per_second')


class TextSerializer(ModelSerializer):

    class Meta:
        model = Text
        fields = '__all__'


class CustomRelatedField(RelatedField):
    def to_internal_value(self, data):
        pass

    def to_representation(self, value):
        if isinstance(value, Video):
            serializer = VideoSerializer(value)
        elif isinstance(value, Audio):
            serializer = AudioSerializer(value)
        elif isinstance(value, Text):
            serializer = TextSerializer(value)
        else:
            raise Exception('Invalid type of content {}'.format(str(type(value))))

        return serializer.data


class ContentSerializer(ModelSerializer):

    content = CustomRelatedField(read_only=True, source='content_object')
    content_type = SerializerMethodField()
    url = SerializerMethodField()

    def get_url(self, obj):
        request = self.context['request']
        return obj.get_detail_url(request)

    def get_content_type(self, obj):
        return obj.content_type.model

    class Meta:
        model = Content
        fields = ('id', 'counter', 'order', 'title', 'content', 'url', 'content_type')


class PageListSerializer(ModelSerializer):

    content = ContentSerializer(many=True)

    class Meta:
        model = Page
        fields = '__all__'


