from django.contrib import admin
from django.contrib.contenttypes import admin as c_admin

from content.models import (
    Audio,
    Content,
    Counter,
    Page,
    Text,
    Video,
)


class PageAdmin(admin.ModelAdmin):
    search_fields = ['content__title__istartswith']


class ContentAdmin(admin.ModelAdmin):
    search_fields = ['content__title__istartswith']


admin.site.register(Audio)
admin.site.register(Content, ContentAdmin)
admin.site.register(Counter)
admin.site.register(Page, PageAdmin)
admin.site.register(Text)
admin.site.register(Video)
