from mediaserver.celery import app
from django.db.models.aggregates import Count
from django.db.models import F
from django.db import transaction
from content.models import Content, Counter, NEW, PROCESSING


@app.task(bind=True)
def count_classified_views(self):
    simultaneous_update = 100
    with transaction.atomic():
        ids = list(
            Counter.objects.select_for_update().filter(state=NEW).values_list('id', flat=True)[0:simultaneous_update])
        Counter.objects.filter(id__in=ids).update(state=PROCESSING)
        aggregated_views = Counter.objects.filter(id__in=ids).values('content_id').annotate(Count('content_id'))

        for data in aggregated_views:
            Content.objects.filter(pk=data['content_id']).update(counter=F('counter') + data['content_id__count'])

        Counter.objects.filter(id__in=ids).delete()
