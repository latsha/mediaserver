**Список всех страниц**

`GET` http://localhost:8000/content/list/

Ответ на запрос 
```
{
    "count":1,
    "next":null,
    "previous":null,
    "results":[
        {
            "id":1,
            "content":[
            {
                "id":1,
                "counter":24,                              # Кол-во просмотров
                "order":1,                                 # Порядок выдачи
                "title":"123",
                "content":
                {
                    "video_link":"http://hello.com",
                    "subtitle_link":"http://hello.org"
                },
                "url":"http://localhost:8000/content/1/",  # Ссылка для просмотра деталей
                "content_type":"video"                     # Признак типа контента
            }]
        }
    ]
}
```            

**Детали контента**

`GET` http://localhost:8000/content/:pk/

Ответ на запрос
```
{
    "id":1,
    "counter":24,
    "order":1,
    "title":"123",
    "content":
    {
        "video_link":"http://hello.com",
        "subtitle_link":"http://hello.org"
    },
    "url":"http://localhost:8000/content/1/",
    "content_type":"video"
}```
